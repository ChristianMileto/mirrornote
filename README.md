  #MirrorNote


MirrorNote is made by: 

Christian Mileto,
Silvio Fosso

It's an OCR and TTS App, using Google Vision. An app for university students who want to take notes and avoid the tedious act of writing; in a fast and efficient way.



![](Video/video.mp4)