

import UIKit
import SwipeCellKit
import ChameleonFramework
struct Helper {
   static var table = UITableView()
    static var array = [String]()
    static var dic = Dictionary<String,Any>()
    static var dialog = String()
}
class NotesTableViewController: UITableViewController, SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else {
            return nil
        }
        let deleteAction = SwipeAction(style:.destructive,title: "Delete") {
            action, indexPath in
            Helper.array.remove(at: indexPath.row)
            tableView.reloadData()
        }
        return [deleteAction]
    }

    //PlaceHolder Array, Just to SetUp The Drawer view.

    var notesArray = [String]()
    @IBOutlet weak var notesTableView: UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notesTableView.dataSource = self
        notesTableView.delegate = self
        tableView.rowHeight = 66
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        Helper.table = notesTableView
        if let variable = UserDefaults.standard.dictionary(forKey: "Notes"){
            Helper.dic = variable
         
            variable.forEach({key,value in
              
                Helper.array.append(key)
            })
        }else{
        Helper.array = notesArray
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Helper.array.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",for:indexPath) as! SwipeTableViewCell
        cell.delegate = self
        // Configure the cell...
        print(Helper.array[indexPath.row])
        cell.textLabel?.text = Helper.array[indexPath.row]
        let color2 = UIColor.flatSkyBlue()?.darken(byPercentage: CGFloat(indexPath.row)/CGFloat(Helper.array.count)*0.25)
        cell.backgroundColor = color2!
      
        return cell
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Notes"
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {

    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let testo = Helper.array[indexPath.row]
        Helper.dic.forEach({ key,value in
            if key == testo{
              
                Helper.dialog = value as! String
                ViewController.event.trigger(eventName: "changeText")
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
        })
    }
   


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}
