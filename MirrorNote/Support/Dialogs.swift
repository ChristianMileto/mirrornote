

import Foundation

//MARK:- Dialog strings

let introductionString : String = "Hello, how are you? I'm Mamuozj, and I'm here to help you learn without wasting time. Please, press the camera button and start scanning! Then, you'll find your Notes in the drawer down here! Enjoy your experience!"


//MARK:- Alert Menu strings
let pickName : String = "Pick a name for this note!"
let addItem : String =  "Add note!"
let eraseItem : String = "Undo!"
let placeHolder : String = "Math Notes 00-00-00"
