//
//  PGActions.swift
//  MirrorNote
//
//  Created by Pierluigi Rizzu on 25/02/2020.
//  Copyright © 2020 Pierluigi Rizzu. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation


let synthesizer = AVSpeechSynthesizer()

func greetPlayer (label:UILabel,chImgView : UIImageView)
{
    label.minimumScaleFactor = 0.5
    label.numberOfLines = 0
    label.setTextWithTypeAnimation(typedText: introductionString, imgView: chImgView)

}

func convertPositionToDrawer(view : UIView,position : CGFloat) -> CGFloat{
    return view.frame.height - position
}
func contLine() -> CGFloat{
    let molt = 60 * Helper.array.count
    return CGFloat(50+molt)
}

func readFromScannedText (textToRead:String)
{
    
    let readText = AVSpeechUtterance(string: textToRead)
    readText.voice = AVSpeechSynthesisVoice(language: "en-US")
    readText.rate = 0.5
    
    synthesizer.speak(readText)
}
func stopSpeak()
{
    synthesizer.stopSpeaking(at: .immediate)
}



func alertWithTFBuilder (testo : String) -> UIAlertController
{
    var textField = UITextField()
   let alert = UIAlertController(title: pickName, message: "", preferredStyle: .alert)
    let addAction = UIAlertAction(title: addItem, style: .default) { (UIAlertAction) in
        print("Note Added")
       
      
    Helper.dic[textField.text!] = testo
   
    UserDefaults.standard.setValue(Helper.dic, forKey: "Notes")
        Helper.array.append(textField.text!)
        Helper.table.reloadData()
       
        //Here you'll find the title to add to userDefaults
        print(textField.text!)

    }

    let deleteAction = UIAlertAction(title: "Undo!", style: .destructive) { (UIAlertAction) in
        print("NOPE")
    }

    alert.addAction(addAction)
    alert.addAction(deleteAction)

    alert.addTextField { (alertTextField) in
        alertTextField.placeholder = placeHolder
        textField = alertTextField
    }

    return alert


}

